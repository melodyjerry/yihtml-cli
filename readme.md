# yihtml-cli (易网页)

`yihtml-cli` 是一个基于 `gulp` 的传统 `html`, `css`, `js` 静态多页面打包工具。

# 特点

1. 默认支持公共 html 模板，避免写重复的 html
2. 默认支持 css 的 scss 写法（不支持 less,stylus 等）
3. 默认支持 js 的 babel 转译,可以使用 ES6 语法
4. 修改代码，自动刷新网页
5. 一键生成页面的 html 、css、js 文件模板
6. 一键删除页面的 html 、css、js 文件
7. 支持开关控制是否进行 js 转译和 source-map 生成

# 如何使用

`yihtml-cli` 自带了一个简单的项目开发模板，请基于此模板进行自己的想开发和改造，否则 `yihtml-cli` 将会无法使用。

## 安装 `yitool-cli`

> yitool-cli 是 [易编程科技](https://yicode.tech) 旗下软件开发生态下的一个专门用于生态内项目模板下载、git 提交统计、npm 镜像切换等功能的命令行工具。

```bash
# npm安装方式
npm install -g @yicode/yitool-cli

# pnpm 安装方式
pnpm add -g @yicode/yitool-cli
```

## 下载 `yihtml-template` 项目模板

> 安装 `@yicode/yitool-cli` 完毕后，创建项目文件，进入到该目录下，直接运行 `yitool` 命令，选择 `template` 指令，回车。

```shell
PS D:\codes\git\chensuiyi\yihtml-cli\test> yitool
? 请选择一个命令
  git          git提交数据可视化
  npm          切换npm源地址
❯ template     下载项目模板
  zip          创建压缩包
  version      查看版本信息
```

> 选择 `静态原生模板 yihtml` ，回车。

```shell
PS D:\codes\git\chensuiyi\yihtml-cli\test> yitool
? 请选择一个命令 template     下载项目模板
? 请选择要下载的模板
  接口模板                           (yiapi-free)
  uniapp模板                       (yiuni-vue3)
  uniapp模板                       (yiuni-vue2)
❯ 静态原生模板                         (yihtml)
  基础模板                           (yibase-vue2)
  基础模板                           (yibase-vue3)
  后台模板                           (yiadmin-vue2)
```

> 下载成功后，提示如下

```shell
PS D:\codes\git\chensuiyi\yihtml-cli\test> yitool
? 请选择一个命令 template     下载项目模板
? 请选择要下载的模板 静态原生模板                         (yihtml)
✔ 下载成功
```

## 运行项目

> 下载项目使用的是 `@yicode/yitool-cli` 工具，运行项目使用 `@yicode/yihtml-cli` （易网页） 静态多页面开发脚手架。

```bash
# npm安装方式
npm install -g @yicode/yihtml-cli

# pnpm 安装方式
pnpm add -g @yicode/yihtml-cli
```

> 安装 `@yicode/yihtml-cli` 完毕后，在项目下执行 `yihtml`，查看命令帮助

```shell
PS D:\codes\git\chensuiyi\yihtml-cli\test> yihtml
使用说明: yihtml-cli [选项] [命令]

选项:
  -v, --version  显示yihtml版本
  -h, --help     显示帮助信息

命令:
  new [选项]       自动生成指令
  del [选项]       自动删除指令
  build          发布环境打包
  dev [选项]       启动开发环境
  help [命令]      显示命令帮助
```

> 使用 `yihtml dev` 运行项目

```shell
PS D:\codes\git\chensuiyi\yihtml-cli\test> yihtml dev
开发环境启动中
开发环境启动完毕
[Browsersync] Access URLs:
 -------------------------------------
       Local: http://localhost:3000
    External: http://172.24.240.1:3000
 -------------------------------------
          UI: http://localhost:3001
 UI External: http://localhost:3001
 -------------------------------------
[Browsersync] Serving files from: D:\codes\git\chensuiyi\yihtml-cli\test\dist
```

按住 `ctrl` 键，鼠标单机 `Local` 处的链接，就会通过默认浏览器打开项目，此时修改项目代码，浏览器会自动刷新。

# 项目架构介绍

```bash
├───📁 css/ # 跟页面一一对应的css文件
│   ├───📄 index.scss
│   └───📄 test1.scss
├───📁 images/
│   └───📁 1/
├───📁 js/ # 跟页面一一对应的js文件
│   ├───📄 index.js
│   └───📄 test1.js
├───📁 public/ # 页面的公共css js 文件
│   ├───📁 css/
│   ├───📁 fonts/
│   └───📁 js/
├───📁 static/ # 静态文件，不进行任何预编译和转译操作，存放jquery等第三方库
│   └───📁 js/
├───📁 styles/ # scss变量文件，使用yihtml new 页面名称，对应的css会自动导入此变量文件
│   └───📄 variable.scss
├───📁 tpls/ # 页面的公共html部分
│   ├───📄 head.html
│   └───📄 script.html
├───📄 index.html # index.html页面
└───📄 test1.html # test.html 页面
```

# 打包项目

> 使用 `yihtml build` 即可，默认打包后的文件放在 `dist` 目录中，打包完毕，上传到服务器即可

```shell
PS D:\codes\git\chensuiyi\yihtml-cli\test> yihtml build
发布环境资源构建中，请耐心等待...
发布环境资源打包完毕
```

# 创建新页面

> 使用 `yihtml new -p 页面名称`，即可创建 `css/news.scss`、`js/news.js`、`news.html` 三个配套的文件，且`news.html` 页面会自动导入对应的 `css` 和 `js` 文件。

```shell
PS D:\codes\git\chensuiyi\yihtml-cli\test> yihtml new -p news
news 页面创建成功
```

# 删除页面

> 使用 `yihtml del -p 页面名称`，将会自动删除对应的 `css/news.scss`、`js/news.js`、`news.html` 文件。

```shell
PS D:\codes\git\chensuiyi\yihtml-cli\test> yihtml del -p news
news 页面删除成功
```

## 仓库地址

> [gitee https://gitee.com/yicode-team/yihtml-cli](https://gitee.com/yicode-team/yihtml-cli)

> [github https://github.com/yicode-team/yihtml-cli](https://github.com/yicode-team/yihtml-cli)

# 授权声明

> 本项目使用 `GPL v3` 协议开源，可免费无偿使用及商用，但不可修改本项目重新发布，有使用问题，请联系作者处理。

# 易编程工作室介绍

![picture 1](https://s2.loli.net/2022/09/16/VKkZq5Bb632vsuy.png)

易编程工作室，由独立开发者[陈随易 https://chensuiyi.com](https://chensuiyi.com) 管理和维护，旗下开源了数十个长期维护（最早于 2019 年开始），且已投入生产使用的开源项目。

致力于为中小企业，外包公司，软件工作室和个人等，提供技术上的规范、简化、可扩展、好维护的项目模板、开发工具等，任何使用问题，请添加联系方式进行反馈交流。

-   微信：c91374286
-   QQ：24323626
-   邮箱：bimostyle@qq.com

## 社区交流

项目相关讨论交流，请添加作者微信 `c91374286` 入群。
